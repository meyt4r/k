ngn/k is a simple fast vector programming language
license: GNU AGPLv3 (v3 only) - https://codeberg.org/ngn/k/blob/master/LICENSE
build: CC=clang-12 make -e  # or CC=gcc-10
usage: rlwrap ./k repl.k
online: https://ngn.bitbucket.io/k

a.h  common header
g.h  header generated by g.k
0.c  syscalls and main()
m.c  memory manager
p.c  parser
b.c  bytecode compiler and virtual machine
k.c  eval, apply, amend, drill: . @
h.c  shape-related: x#y ,x x,y
o.c  order and equivalence
s.c  object to string: $x `k@x
f.c  find, random: x?y
1.c  monadic arithmetic
2.c  dyadic  arithmetic except + and *
3.c  dyadic  arithmetic + and *
i.c  i/o and \cmds
v.c  the rest of the verbs
w.c  adverbs
j.c  json: `j@x `j?x
x.c  serialization: `@x `?x
c.c  cryptography
e.c  error handling
t/   unit tests
g/   https://codegolf.stackexchange.com/
e/   https://projecteuler.net/
a19/ https://adventofcode.com/2019
a20/ https://adventofcode.com/2020
o/   build tmp
web/ browser ui
